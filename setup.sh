#! /bin/bash

navn="puppet"
testvar="ingeninput"
privkey="auth/daniebru.pem"
repodocker="git clone https://daniebru@bitbucket.org/daniebru/test.git"
swarmnavn="swarmbro"



# Openstack tilgang og setter opp stack
source auth/openstack.sh
echo "Starter stack med navn $navn"
openstack stack create -t egen/ellera_infrastruktur.yaml -e egen/environment.yaml $navn


# Sjekker at stacken er i orden. Exit dersom ikke
until [ $testvar = "CREATE_COMPLETE" ]; do
                sleep 10s
                testvar=$(openstack stack list | grep $navn | awk '{print $6}')
                echo $testvar
                if [ $testvar = "CREATE_FAILED" ]; then
                                                        openstack stack delete $navn
							exit 1
							echo "Stack failed"

		fi
	done


# VM-navn og floating IP for SSH
vm1=$(openstack server list | grep webserver_puppet | awk ' { print $4 }')
vm2=$(openstack server list | grep manager_puppet | awk ' { print $4 }')

ipvm1=$(openstack server list | grep $vm1 | awk ' { print $9}')
ipvm2=$(openstack server list | grep $vm2 | awk ' { print $9}')

echo "VM: $vm1, IP: $ipvm1"
echo "VM: $vm2, IP: $ipvm2"


# Enkel SSH-tilgang

cat <<EOF > manager.sh
#! /bin/bash

echo "Valg"
echo "Webserver (1)"
echo "Manager (2)"
read valg


case \$valg in
1)
        ssh -i $privkey ubuntu@$ipvm1;;
2)
        ssh -i $privkey ubuntu@$ipvm2;;
*)      echo "Velg manager eller webserver";;

esac
EOF

# Gi stacken tid til å komme igang
echo "Starter ssh sjekk"
sleep 20s

ssh -i $privkey -q -o ConnectTimeout=5 ubuntu@$ipvm1 exit
con=$?
until [ $con == 0 ]
do
	sleep 5s
	echo "Tester connection $vm1"
        ssh -i $privkey -q -o ConnectTimeout=5 ubuntu@$ipvm1 exit
        con=$?
	echo "$con"
done
echo "$vm1 OK"

sleep 5s
ssh -i $privkey -q -o ConnectTimeout=5 ubuntu@$ipvm2 exit
con=$?
echo "Tester connection $vm2"
echo "$con"
if [ $con == 0 ]
then
        echo "$vm2 OK"
else echo "Noe gikk galt med SSH"; exit 1
fi

# Kopierer over docker installasjon til begge VM og installerer
scp -i $privkey -q egen/dockerconfig/installdocker.sh  ubuntu@$ipvm1:/home/ubuntu &
scp -i $privkey -q egen/dockerconfig/installdocker.sh  ubuntu@$ipvm2:/home/ubuntu &

sleep 5s

ssh -i $privkey -q ubuntu@$ipvm1 "sudo chmod 700 installdocker.sh"
echo "chmod vm1"
sleep 2s
echo "installerer vm1"
ssh -i $privkey -q ubuntu@$ipvm1 "sudo nohup ./installdocker.sh"
echo "Ferdig vm1"

ssh -i $privkey -q ubuntu@$ipvm2 "sudo chmod 700 installdocker.sh"
echo "chmod vm2"
sleep 2s
echo "installerer vm2"
ssh -i $privkey -q ubuntu@$ipvm2 "sudo nohup ./installdocker.sh"
echo "Ferdig vm2"

# Kloner repo for konfigurasjonsfiler

ssh -i $privkey -q ubuntu@$ipvm2 "$repodocker"
echo "Repo $repodocker klonet"

# Docker swarm oppsett

ssh -i $privkey -q ubuntu@$ipvm2 << EOF
cd test/docker
sudo chmod 700 setupswarm.sh
./setupswarm.sh
EOF

# Docker images sjekk

image=$(ssh -i $privkey -q ubuntu@$ipvm2 "sudo docker images | grep jenkins" | awk ' NR==1 { print $1}')
until [ "$image" == "jenkins" ]
do
	sleep 15s
	image=$(ssh -i $privkey -q ubuntu@$ipvm2 "sudo docker images | grep jenkins" | awk ' NR==1 { print $1}')
	echo "Sjekker docker images for jenkins. "
	echo "Image funnet: $image"
done

echo "Setupswarm script ferdig"

# Venter på docker swarm init på manager
up=$(ssh -i $privkey -q ubuntu@$ipvm2 "sudo docker node list | grep manager-puppet" | awk '{ print $4}')
until [  "$up" == "Ready" ]
do
	echo "Venter ferdig swarm-manager"
        sleep 5s
done

# Lagrer swarm token i $token
token=$(ssh -i "$privkey" -q ubuntu@"$ipvm2" "sudo docker swarm join-token worker" | grep "docker swarm")
echo "Token er $token"

echo "Swarm manager oppe"

# Sender token til webserver

ssh -i $privkey -t ubuntu@$ipvm1 "sudo $token" 

worker="$(ssh -i $privkey -q ubuntu@$ipvm2 "sudo docker node list | grep webserver-puppet" | awk '{ print $3}')"

until [ $worker == "Ready" ]
do
        sleep 5s
	worker="$(ssh -i $privkey -q ubuntu@$ipvm2 "sudo docker node list | grep webserver-puppet" | awk '{ print $3}')"
        echo "Venter på swarm worker"
done
echo "Swarm worker Ready"

# Deployer stack
ssh -i $privkey -q ubuntu@$ipvm2 "sudo docker stack deploy --compose-file test/docker/docker-compose.yaml $swarmnavn"

echo "Stack klar"

