puppet module install puppetlabs-apt --version 6.2.1
puppet module install puppetlabs-translate --version 1.1.0
puppet module install puppetlabs-powershell --version 2.2.0
puppet module install puppetlabs-reboot --version 2.0.0
puppet module install puppetlabs-stdlib --version 5.1.0
puppet module install puppetlabs-docker --version 3.2.0
